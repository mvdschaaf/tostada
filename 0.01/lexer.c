#include "global.h"
#include "error.h"
#include "symbol.h"

char lexBuf[BSIZE];

int isLetterOrDigit(char);
int isIdentifierLetter(char);
int isDigit(char);
int isUpperCaseLetter(char);
int isLowerCaseLetter(char);
int isExtendedDigit(char);

int t = -1;

int lexan(FILE *file)
{
  int b,state,base,exponent;

  state = base = numTokenVal = exponent = 0;

  for(b=0;b<BSIZE;++b)
    lexBuf[b] = 0;

  b = 0;

  if(t == -1)
    t = fgetc(file);

  while(1)
  {
    #ifdef DEBUG
    fprintf(stderr,"State: %d, t: %d\n",state,t);
    #endif
    if(state == 0)
    {
      if(t == ' ')
      {
        t = fgetc(file);
      } 
      else if(t == '-')
      {
        t = fgetc(file);
        if(t == '-')
        {
          state = 1;
        }
        return '-';
      }
      else if(t == '"')
      {
        t = fgetc(file);
        state = 2;
      }
      else if(isIdentifierLetter(t))
      {
        state = 3;
      }
      else if(isDigit(t))
      {
        state = 5;
      }
      else if(t == '\r' || t == '\n')
      {
        if(t == '\r')
        {
          t = fgetc(file);
          if(t == '\n')
            t = fgetc(file);
        }
        else
          t = fgetc(file);
        lineNumber++;
      }
      else if(t == '&' || t == '\'' || t == '(' || t == ')' || t == '+' || t == ',' || t == ';' || t == '|')
      {
        int retVal = t;
        t = fgetc(file);
        return retVal;
      }
      else if(t == EOF)
      {
        return t;
      }
      else if(t == '=' || t == '.' || t == '*' || t == ':' || t == '/' || t == '=' || t == '<' || t == '>')
      {
        char first;
        first = t;

        t = fgetc(file);
        if(first == '=' && t == '>')
        {
          t = fgetc(file);
          return ARROW;
        } 
        else if(first == '.' && t == '.')
        {
          t = fgetc(file);
          return DOUBLEDOT;
        }
        else if(first == '*' && t == '*')
        {
          t = fgetc(file);
          return DOUBLESTAR;
        }
        else if(first == ':' && t == '=')
        {
          t = fgetc(file);
          return BECOMES;
        }
        else if(first == '/' && t == '=')
        {
          t = fgetc(file);
          return NOTEQUAL;
        }
        else if(first == '>' && t == '=')
        {
          t = fgetc(file);
          return NOTLESS;
        }
        else if(first == '<' && t == '=')
        {
          t = fgetc(file);
          return NOTGREATER;
        }
        else if(first == '<' && t == '<')
        {
          t = fgetc(file);
          return LEFTLABEL;
        }
        else if(first == '>' && t == '>')
        {
          t = fgetc(file);
          return RIGHTLABEL;
        }
        else if(first == '<' && t == '>')
        {
          t = fgetc(file);
          return BOX;
        }
        return first;
      }
      else
      {
        fprintf(stderr,"Unknown character '%d'\n",t);
        exit(1);
      }
    }
    else if(state == 1) /* Lexing a comment, saw -- */
    {
      while(t != '\n' && t != '\r' && t != EOF)
      {
        t = fgetc(file);
      }
      state = 0;
    }
    else if(state == 2) /* Lexing a string, saw " */
    {
      if(t == '"')
      {
        t = fgetc(file);
        if(t == '"')
        {
          lexBuf[b++] = t;
          t = fgetc(file);
        }
        else
        {
          char *buffer = malloc(b+1);
          strcpy(buffer,lexBuf);
          strTokenVal = buffer;

          t = fgetc(file);

          return STR;
        }
      }
      else if (t == '\n' || t == '\r')
      {
      }
      else
      {
        lexBuf[b++] = t;
        t = fgetc(file);
      }
    }
    else if(state == 3) /* Lexing an identifier */
    {
      if (isLetterOrDigit(t))
      {
        lexBuf[b++] = t;
        t = fgetc(file);
      } 
      else if (t == '_')
      {
        lexBuf[b++] = t;
        t = fgetc(file);
        state = 4;
      }
      else if (t == ' ' || t == '\t' || t == '\r' || t == '\n' || t == ',' || t == ';' || t == '(' || t == ')' || t == '=' || t == '<' || t == '>' || t == '+' || t == '-' || t == '*' || t == '/' || t == '.')
      {
        int p = 0;

        p = lookup(lexBuf);

        if (p == 0)
          p = insert(lexBuf, IDENT);

        numTokenVal = p;

        return symTable[p].token;
      }
      else
      {
        fprintf(stderr,"Unexpected character '%c' at line %d\n",t,lineNumber);
        exit(1);
      }
    }
    else if(state == 4) /* Lexing an identifier, saw _ */
    {
      if(isLetterOrDigit(t))
      {
        lexBuf[b++] = t;
        t = fgetc(file);
        state = 3;
      }
      else
      {
        error("Cannot end identifier with an underscore");
      }
    }
    else if(state == 5) /* Lexing a numeric literal */
    {
      if(isDigit(t))
      {
        numTokenVal = numTokenVal * 10 + (t-'0');
        t = fgetc(file);
      }
      else if(t == '_')
      {
        t = fgetc(file);
        state = 6;
      }
      else if(t == 'E')
      {
        t = fgetc(file);
        state = 10;
      }
      else if(t == '.')
      {
        t = fgetc(file);
        state = 7;
      }
      else if(t == '#')
      {
        base = (int)numTokenVal;
        numTokenVal = 0;
        if(base < 2 || base > 16)
          error("Base must be between 2 and 16");

        state = 14;
      }
      else
      {
        return NUM;
      }
    }
    else if(state == 7) /* Lexing a number, saw a . */
    {
      base = 10;
      if(isDigit(t))
        state = 8;
      else
        error("Expected a number after a .");
    }
    else if(state == 8) /* Lexing a number, saw a . */
    {
      if(isDigit(t))
      {
        numTokenVal = numTokenVal + (t-'0')/base;
        base *= 10;
        t = fgetc(file);
      }
      else if(t == '_')
      {
        t = fgetc(file);
        state = 9;
      }
      else if(t == 'E')
      {
        t = fgetc(file);
        state = 10;
      }
      else
      {
        return NUM;
      }
    }
    else if(state == 10) /* Lexing a number, saw an E */
    {
      if(isDigit(t))
        state = 12;
      else if(t == '+' || t == '-')
        state = 11;
      else
        error("Expected a sign or number after E");
    }
    else if(state == 11) /* Lexing a number, saw an E and a sign */
    {
      if(t == '-')
      {
        t = fgetc(file);
        if(isDigit(t))
        {
          exponent = -(t-'0');
          t = fgetc(file);
          state = 12;
        }
        else
          error("Expected a number after E-");
      }
      else
      {
        t = fgetc(file);
        if(isDigit(t))
          state = 12;
        else
          error("Expected a number after E+");
      }
    }
    else if(state == 12) /* Lexing a number, saw an E */
    {
      if(isDigit(t))
      {
        if(exponent >= 0)
          exponent = exponent*10+(t-'0');
        else
          exponent = exponent*10-(t-'0');
        t = fgetc(file);
      }
      else if(t == '_')
      {
        t = fgetc(file);
        state = 13;
      }
      else
      {
        int i;
        if(exponent < 0)
        {
          for(i=exponent;i>=0;--i)
            numTokenVal/=10;
        }
        else
        {
          for(i=0;i<exponent;++i)
            numTokenVal*=10;
        }
        return NUM;
      }
    }
    else if(state == 14)
    {
      error("Based numbers not implemented yet");
    }
    else if(state == 6 || state == 9 || state == 13 || state == 23) /* Lexing a number, saw an underscore */
    {
      if(isDigit(t))
        state--;
      else
        error("Expected a number after an underscore");
    }

    else if(state == 16 || state == 18) /* Lexing a base number, saw an underscore */
    {
      if(isExtendedDigit(t))
        state--;
      else
        error("Expected a number after an underscore");
    }
    else
    {
      fprintf(stderr,"Illegal state %d\n",state);
      exit(1);
    }
  }
}

int isLetterOrDigit(char c)
{
  return isIdentifierLetter(c) || isDigit(c);
}

int isIdentifierLetter(char c)
{
  return isUpperCaseLetter(c) || isLowerCaseLetter(c);
}

int isDigit(char c)
{
  return c >= '0' && c <= '9';
}

int isExtendedDigit(char c)
{
  return isDigit(c) || (c >= 'A' && c <= 'F');
}

int isUpperCaseLetter(char c)
{
  return c >= 'A' && c <= 'Z';
}

int isLowerCaseLetter(char c)
{
  return c >= 'a' && c <= 'z';
}
