#ifndef __EMITTER___H_
#define __EMITTER___H_

void setupEmitter(FILE *);
void emitMain(char *);
void emitProcedureStart(char *,int,char **,char **);
void emitProcedureEnd();
void emitCallProcedure(char *);

void emitFunctionStart(char *,char *,int,char **,char**);
void emitFunctionEnd();

void emitNum(double);
void emitString(char*);

#endif
