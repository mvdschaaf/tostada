#include "global.h"
#include "error.h"

#define STRMAX 999

char lexemes[STRMAX];
int  lastChar = -1;
struct entry symTable[SYMMAX];
int  lastEntry = 0;

int lookup(char s[])
{
  int p;
  for(p = lastEntry; p > 0; p = p-1)
    if (strcmp(symTable[p].lexPtr,s) == 0)
      return p;
  return 0;
}

int insert(char s[], int tok)
{
  int len;
  len = strlen(s);
  if (lastEntry+1 >= SYMMAX)
    error("Symbol table full");
  if (lastChar + len + 1 >= STRMAX)
    error("Lexemes array full");

  lastEntry = lastEntry + 1;

  symTable[lastEntry].token = tok;
  symTable[lastEntry].lexPtr = &lexemes[lastChar+1];
  lastChar = lastChar + len + 1;

  strcpy(symTable[lastEntry].lexPtr,s);

  return lastEntry;
}
