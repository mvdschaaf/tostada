#include "global.h"
#include "symbol.h"

struct entry keywords[] = {
    {"begin",     BEGIN},
    {"end",       END},
    {"function",  FUNCTION},
    {"procedure", PROCEDURE},
    {"is",        IS},
    {"return",    RETURN},
    {"null",      NULL_VAL},
    {0,       0}
};

void init()
{
  struct entry *p;
  for (p = keywords; p->token; p++)
    insert(p->lexPtr, p->token);
}
