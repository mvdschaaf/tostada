#include <stdio.h>
#include "lexer.h"
#include "global.h"

extern int lineNumber;

int main()
{
  int token;

  do
  {
    token = lexan(stdin);
    if(token == '\r' || token == '\n')
      lineNumber++;
    printf("%d ",token);
    if(token == VAR)
      printf("%s",symTable[(int)numTokenVal].lexPtr);
    if(token == STR)
    {
      printf("%s",strTokenVal);
      free(strTokenVal);
    }
    if(token == NUM)
      printf("%lf",numTokenVal);
    printf("\n");
  } while (token != EOF);
 
  return 0;
}
