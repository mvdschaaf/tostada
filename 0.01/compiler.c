#include <stdio.h>
#include "global.h"
#include "parser.h"
#include "init.h"

int main(int argc,char **argv)
{
  FILE *file,*outf;

  if(argc != 2)
  {
    fprintf(stderr,"Expected one argument\n");
    exit(1);
  }

  file = fopen(argv[1],"r");
  if(file == NULL)
  {
    fprintf(stderr,"Cannot open file %s\n",argv[1]);
    exit(1);
  }

  outf = fopen("interm.ll","w");
  if(file == NULL)
  {
    fprintf(stderr,"Cannot open file %s for writing\n","interm.ll");
    exit(1);
  }

  init();
  parse(file,outf);

  fclose(file);
  fclose(outf);
  
  return 0;
}
