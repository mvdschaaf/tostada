#include "global.h"

void error(char *m)
{
  fprintf(stderr, "Line %d: %s\n", lineNumber, m);
  exit(1);
}
