#ifndef ___GLOBAL_H___
#define ___GLOBAL_H___
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BSIZE 256 
#define NONE -1
#define EOS '\0'

#define SYMMAX 200
#define CALLMAX 256

#define NUM          256
#define STR          257
#define DOUBLEDOT    258
#define DOUBLESTAR   259
#define BECOMES      260
#define NOTEQUAL     261
#define NOTLESS      262
#define NOTGREATER   263
#define LEFTLABEL    264
#define RIGHTLABEL   265
#define BOX          266
#define ARROW        267
#define IDENT        268
#define BEGIN        269
#define END          270
#define FUNCTION     271
#define PROCEDURE    272
#define IS           273
#define RETURN       274
#define NULL_VAL     275

char *strTokenVal;
double numTokenVal;

int lineNumber;
int tempVarNum;

struct entry {
  char *lexPtr;
  int token;
};

struct entry symTable[SYMMAX];

#endif
