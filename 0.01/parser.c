#include <stdio.h>
#include "global.h"
#include "lexer.h"
#include "error.h"
#include "emitter.h"

FILE *file;

int lookAhead;

void compilationUnit();
void contextClause();
void libraryItem();
void libraryUnitBody();
void subprogramBody();
void formalPart();

void sequenceOfStatements();
void nullStatement();
void procedureCallStatement();

void expression();
void simpleExpression();
void primary();
void factor();
void term();
void relation();

void match(int);

char *lexstr();

void assertSameName(char*,char*);

int isFunction(char*);

void parse(FILE *currFile,FILE *outfile)
{
  file = currFile;
  lineNumber = 1;

  lookAhead = lexan(file);
  setupEmitter(outfile);
  compilationUnit();
}

void compilationUnit()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"compilationUnit\n");
  #endif

  contextClause();
  libraryItem();
}

void contextClause()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"contextClause\n");
  #endif
}

void libraryItem()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"libraryItem\n");
  #endif
  libraryUnitBody();
}

void libraryUnitBody()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"libraryUnitBody\n");
  #endif
  subprogramBody();
}

void subprogramBody()
{
  char *funcName;
  char *compName;
  char *returnType;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"subProgramBody\n");
  #endif

  if(lookAhead == PROCEDURE)
  {
    match(PROCEDURE);
    if(lookAhead != IDENT)
      error("Expected identifier");
    funcName = lexstr();
    match(IDENT);

    if(lookAhead == '(')
      formalPart();

    emitProcedureStart(funcName,0,0,0);
  }
  else if(lookAhead == FUNCTION)
  {
    match(FUNCTION);
    if(lookAhead != IDENT)
      error("Expected identifier");
    funcName = lexstr();
    match(IDENT);

    if(lookAhead == '(')
      formalPart();

    match(RETURN);
    
    returnType = lexstr();
    match(IDENT);

    emitFunctionStart(funcName,returnType,0,0,0);
  }
  else
    error("Expected 'procedure' or 'function'");

  match(IS);
  match(BEGIN);

  sequenceOfStatements();

  match(END);

  compName = lexstr();
  match(IDENT);
  assertSameName(compName,funcName);

  match(';');
  emitProcedureEnd();
  emitMain(funcName);
}

void sequenceOfStatements()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"sequenceOfStatements\n");
  #endif

  while(lookAhead != END)
  {
    if(lookAhead == NULL_VAL)
      nullStatement();
    else if(isFunction(lexstr()))
      procedureCallStatement();
    else
      match(lookAhead);
  }
}

void nullStatement()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"nullStatement\n");
  #endif

  match(NULL_VAL);
  match(';');
}

void procedureCallStatement()
{
  char *name;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"procedureCallStatement\n");
  #endif

  name = lexstr();
  match(IDENT);
  
  if(lookAhead == '(')
  {
    match('(');
    expression();
    while(lookAhead == ',')
    {
      match(',');
      expression();
    }
    match(')');
  }

  match(';');

  emitCallProcedure(name);
}

void expression()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"expression\n");
  #endif

  relation();
}

void relation()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"relation\n");
  #endif

  simpleExpression(); 
}

void simpleExpression()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"simpleExpression\n");
  #endif
 
  term();
}

void term()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"term\n");
  #endif

  factor();
}

void factor()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"factor\n");
  #endif

  primary();
}

void primary()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"primary\n");
  #endif
  if(lookAhead == NUM)
    emitNum(NUM);
  else if(lookAhead == STR)
    emitString(lexstr());
  else if(lookAhead == '(')
  {
    match('(');
    expression();
    match(')');
  }
} 

void formalPart()
{
  error("Function parameters not implemented yet");
}

void assertSameName(char *compName,char *name)
{
  if(strcmp(compName,name) != 0)
    error("Mismatch in names");
}

int isFunction(char *name)
{
  if(strcmp(name,"put") == 0)
    return 1;
  if(strcmp(name,"new_line") == 0)
    return 1;
  return 0;
}

char *lexstr()
{
  return symTable[(int)numTokenVal].lexPtr;
}

void match(int t)
{
  if (lookAhead == t)
  {
    #ifdef DEBUG
      fprintf(stderr,"lookAhead = %d\n",lookAhead);
    #endif
    lookAhead = lexan(file);
  }
  else
  {
    char msg[50];
    sprintf(msg,"Expected %d, found %d\n",t,lookAhead);
    error(msg);
  }
}
