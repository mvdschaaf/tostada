#include "global.h"
#include "error.h"

FILE *outf;

void setupEmitter(FILE *outfile)
{
  outf = outfile;
}

void emitMain(char *funcName)
{
  fprintf(outf,"define i32 @main() #0 {\n");
  fprintf(outf,"call void @%s()\n",funcName);
  fprintf(outf,"ret i32 0\n}\n");
}

void emitProcedureStart(char *name,int numArgs,char **argTypes,char **argNames)
{
  int i;

  fprintf(outf,"define void @%s(",name);
  for(i=0;i<numArgs;++i)
  {
    fprintf(outf,"%s %s",argTypes[i],argNames[i]);
    if(i+1<numArgs)
      fprintf(outf,",");
  }
  fprintf(outf,") #0 {\n");
}

void emitProcedureEnd()
{
  fprintf(outf,"}\n");
}

void emitCallProcedure(char *name)
{
  error("Emit call procedure not implemented yet");
}

void emitFunctionStart(char *name,char *returnType,int numArgs,char **argTypes,char **argNames)
{
  error("Emit function start not implementet yet");
}

void emitNum(double num)
{
  error("Emitting numbers not implemented yet");
}

void emitString(char *str)
{
  error("Emitting strings not implemented yet");
}
